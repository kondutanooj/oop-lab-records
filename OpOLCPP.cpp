#include <iostream>
using namespace std;
class Complex {
private:
	int real, imag;
public:
	Complex(int r = 0, int i = 0)
	{
		real = r;
		imag = i;
	}
	Complex operator+(Complex const& obj1)
	{
		Complex obj2;
		obj2.real = real + obj1.real;
		obj2.imag = imag + obj1.imag;
		return obj2;
	}
	void print() 
	{ 
	        cout << real << " + i" << imag << '\n'; 
	}
};
int main()
{
	Complex c1(10, 5), c2(2, 4);
	Complex c3 = c1 + c2;
	c3.print();
}
