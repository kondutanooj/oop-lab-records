#include<iostream>
using namespace std;
class Box{
    float len,wid,high;
    public:
        void boxArea(float length, float width,float height){
            len=length;
            wid=width;
            high=height;
            cout<<"Area is : "<<2*(len*wid+len*high+wid*high)<<endl;
        }
        void boxVolume(float length, float width, float height);
        inline void display(){
            cout<<"\n\tWELCOME TO THE PROGRAM...\t\n"<<endl;
        }
        friend void displayBoxDimensions(Box);
};
void Box::boxVolume(float length, float width, float height){
    len=length;
    wid=width;
    high=height;
    cout<<"volume is :"<<len*wid*high<<endl;
};
void displayBoxDimensions(Box b){
    cout<<b.len<<" "<< b.wid<<" "<<b.high<<endl;
}
int main()
{
    float l,w,h;
    Box obj;
    obj.display();
    cout<<"Enter the dimensions : "<<endl;
    cin>>l>>w>>h;
    obj.boxArea(l,w,h);
    obj.boxVolume(l,w,h);
    cout<<"Dimensions are as follows :\n";
    displayBoxDimensions(obj);
    return 0;
}
