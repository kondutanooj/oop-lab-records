import fibpackage.*;
import java.util.*;
public class Fibonacci{
    public static void main(String[] args) {
        Fib obj = new Fib();
		Scanner in = new Scanner(System.in);
		System.out.println("Enter the nth number input : "); 
		long num1 = in.nextLong();
		System.out.println("Enter the nth series input : "); 
		long num2 = in.nextLong();
        obj.fibnum(num1);
        obj.fibseries(num2);
    }
}