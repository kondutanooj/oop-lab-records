class Base{
    private int a = 10;
    public int b = 20;
    protected int c =30;
    public int getpriv(){
        return a;
    }
}
class publiclass extends Base{
    public int getprot(){
        return c;
    }
}
public class PPPInheriJAVA{
    public static void main (String[] args) {
        publiclass obj1 = new publiclass();
        System.out.println("\nPUBLIC-MODE\n");
        System.out.println("Public :"+obj1.b);
        System.out.println("Protected :"+obj1.getprot());
        System.out.println("Private :"+obj1.getpriv());
    }
}