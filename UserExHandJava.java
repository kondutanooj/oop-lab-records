import java.util.Scanner;
class InvalidInputException extends Exception {
    public InvalidInputException(String message) {
        super(message);
    }
}
public class UserExHandJava{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a positive number less than 10: ");
        try {
            int num = scanner.nextInt();
            if (num <= 0 || num >= 10) {
                throw new InvalidInputException("Invalid input: " + num);
            }
        } catch (InvalidInputException e) {
            System.out.println("Error: " + e.getMessage());
        }
        scanner.close();
    }
}