class Base{
	public void add(int a,int b){
		System.out.println("Sum="+(a+b));
	}
}
class Child extends Base{
	public void add(double a, double b){
		System.out.println("Sum="+(a+b));
	}
}
public class MethodOLInheriJava{
	public static void main(String[] args){
		Child obj = new Child();
		obj.add(2,3);
		obj.add(-2.3,3.4);
	}
}