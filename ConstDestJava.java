import java.util.*;
class Student{
    String fullName;
    int rollNum;
    double semPerentage;
    String collegeName;
    int collegeCode;
    Student(){}
    void display(){
        Formatter fr = new Formatter();
        fr.format("%.2f",semPerentage);
        System.out.println("\n\n\t*****Details*****\n\n");
        System.out.println("Name : "+fullName);
        System.out.println("Rollnum : "+rollNum);
        System.out.println("Sem-percentage : "+fr);
        fr.close();
        System.out.println("College Name : "+collegeName);
        System.out.println("College Code : "+collegeCode);
    }
    public void finalize(){
       System.out.println("Destroyed!!!");
    }
}
public class ConstDestJava{
    public static void main(String[] arg){
        Student obj = new Student();
         Scanner info = new Scanner(System.in);
         System.out.println("Enter your Name : ");
        obj.fullName= info.next();
        System.out.println("Enter your Rollnum : ");
        obj.rollNum= info.nextInt();
        System.out.println("Enter your Sem-percentage : ");
        obj.semPerentage= info.nextFloat();
         System.out.println("Enter your College name : ");
        obj.collegeName= info.next();
        System.out.println("Enter your College Code : ");
        obj.collegeCode= info.nextInt();
        obj.display();

        obj = null;
        System.gc();
        info.close();
    }
}
