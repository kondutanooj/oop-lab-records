#include <iostream>
using namespace std;
class Base1{
    public:
    void displayBase1(){
        cout<<"Class-Base1"<<endl;
    }
};
class Base2{
    public:
    void displayBase2(){
        cout<<"Class-Base2"<<endl;
    }
};
class Sub1:public Base1{
    public:
    void displaySub1(){
        cout<<"Class-Sub1"<<endl;
    }
};
class Sub2:public Sub1{
    public:
    void displaySub2(){
        cout<<"Class-Sub2"<<endl;
    }
};
class Sub3:public Sub1{
    public:
    void displaySub3(){
        cout<<"Class-Sub3"<<endl;
    }
};
class Sub4:public Base1,public Base2{
    public:
    void displaySub4(){
        cout<<"Class-Sub4"<<endl;
    }
};
class Sub5:public Sub4{
    public:
    void displaySub5(){
        cout<<"Class-Sub5"<<endl;
    }
};
int main(){
    Sub1 obj1;
    Sub2 obj2;
    Sub3 obj3;
    Sub4 obj4;
    Sub5 obj5;
    cout<<"\n***SINGLE-INHERITANCE***\n"<<endl;
    obj1.displayBase1();
    obj1.displaySub1();
    cout<<"\n***MULTIPLE-INHERITANCE***\n"<<endl;
    obj4.displayBase1();
    obj4.displayBase2();
    obj4.displaySub4();
    cout<<"\n***MULTI-LEVEL-INHERITANCE***\n"<<endl;
    obj2.displayBase1();
    obj2.displaySub1();
    obj2.displaySub2();
    cout<<"\n***HIERARCHICAL-INHERITANCE***\n"<<endl;
    cout<<"left-child"<<endl;
    obj2.displaySub1();
    obj2.displaySub2();
    cout<<"right-child"<<endl;
    obj3.displaySub1();
    obj3.displaySub3();
    cout<<"\n***HYBRID-INHERITANCE***\n"<<endl;
    obj5.displayBase1();
    obj5.displayBase2();
    obj5.displaySub4();
    obj5.displaySub5();
}
