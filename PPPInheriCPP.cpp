#include <iostream>
using namespace std;

class Common {
  private:
    int priv = 10;

  protected:
    int prot = 20;

  public:
    int publ = 30;

    // function to access private member
    int getpriv() {
      return priv;
    }
};
// PUBLIC-CLASS
class Publclass : public Common {
  public:
    // function to access protected member from Common
    int getprot() {
      return prot;
    }
};
// PROTECTED-CLASS
class Protclass : protected Common {
  public:
    // function to access protected member from Common
    int getprot() {
      return prot;
    }

    // function to access public member from Common
    int getpubl() {
      return publ;
    }
};
// PRIVATE-CLASS
class Privclass : private Common {
  public:
    // function to access protected member from Common
    int getprot() {
      return prot;
    }

    // function to access private member
    int getpubl() {
      return publ;
    }
};
int main() {
  Publclass obj1;
  Protclass obj2;
  Privclass obj3;
  cout<<"\nPUBLIC-MODE\n"<<endl;
  cout << "Private = " << obj1.getpriv() << endl;
  cout << "Protected = " << obj1.getprot() << endl;
  cout << "Public = " << obj1.publ << endl;
  cout<<"\nPROTECTED-MODE\n"<<endl;
  cout << "Private cannot be accessed." << endl;
  cout << "Protected = " << obj2.getprot() << endl;
  cout << "Public = " << obj2.getpubl() << endl;
  cout<<"\nPRIVATE-MODE\n"<<endl;
  cout << "Private cannot be accessed." << endl;
  cout << "Protected = " << obj3.getprot() << endl;
  cout << "Public = " << obj3.getpubl() << endl;
  return 0;
}
