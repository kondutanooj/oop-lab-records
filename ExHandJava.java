import java.util.*;
public class ExHandJava{
    public static void main(String[] args) {
        float[] arr = { 1, -8, -3 };
        Scanner s = new Scanner(System.in);
        try {
			System.out.println("Enter Array index: ");
            float Number = arr[s.nextInt()];
			System.out.println("Enter dividend: ");
            int a = s.nextInt();
            System.out.println(Number / a);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Error: Array index out of bounds.");
        } catch (InputMismatchException obj) {
            System.out.println("Error: not entered correct input.");
        } catch (ArithmeticException obj) {
            System.out.println("Error: with zero cant't be divided.");
        }
    }
}

