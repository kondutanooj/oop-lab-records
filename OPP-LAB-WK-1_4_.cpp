#include<iostream>
#include<iomanip>
#include<sstream>
#include<istream>
using namespace std;
int main(){
	double n=12.36;
	cout<<"Using_endl,ends,ws,flush"<<endl;
	istringstream s("       CPP Program");
	string l;
	getline(s>>ws,l);
	cout<<l<<endl;
	cout<<"variable a,b,c"<<flush;
	cout<<"\na";
	cout<<"b"<<ends;
	cout<<"c"<<endl;
	cout<<setw(10)<<"OOP's"<<setw(6)<<setfill('*')<<"CPP"<<endl;
        cout<<setprecision(3)<<n<<endl;
	cout<<setprecision(2)<<n<<endl;
	return 0;
}
