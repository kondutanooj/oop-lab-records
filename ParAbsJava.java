abstract class Base{
	abstract void display();
	void hello(){}
}
class Child extends Base{
    void display(){
        System.out.println("Pure abstract message");
    }
}
public class ParAbsJava{
    public static void main (String[] args){
        Child obj = new Child();
        obj.display();
        obj.hello();
    }
}