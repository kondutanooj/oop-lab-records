class Base{
	public void display(){
		System.out.println("Display from Base-class");
	}
}
class Child1 extends Base{
	public void display(){
		System.out.println("Display from Child1-class");
	}
}
class Child2 extends Base{
	public void display(){
		System.out.println("Display from Child2-class");
	}
}
public class MethodORJava{
	public static void main(String[] args){
		Base obj1 = new Base();
		obj1.display();
		Child1 obj2 = new Child1();
		obj2.display();
		Child2 obj3 = new Child2();
		obj2.display();
	}
}