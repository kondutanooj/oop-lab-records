class MutableJava{
    static String name = "MVGR";
    public static void main(String[] args) {
        StringBuffer sb = new StringBuffer("Hello ");
        System.out.println(sb+"--> address: "+System.identityHashCode(sb));
        sb.append(name);
        System.out.println(sb+"--> address: "+System.identityHashCode(sb));
        String str = "Hi, ";
        System.out.println(str+"--> address: "+System.identityHashCode(str));
        str = str + name;
        System.out.println(str+"--> address: "+System.identityHashCode(str));
    }
}

