class Student{
    String collegeName;
    int collegeCode;
    String fullName;
    double semPerentage;
    Student(){
        collegeName="MVGR";
        collegeCode=33;
        System.out.println("College Name : "+collegeName);
        System.out.println("College Code: "+collegeCode);
    }
    Student(String fullName,double semPerentage){
        this.fullName=fullName;
        this.semPerentage=semPerentage;
        System.out.println("Name : "+fullName);
        System.out.println("Sem-Percentage : "+semPerentage);
    }
     public void finalize(){
         System.out.println("Destroyed!!!");
     }
}
public class ParamConstJava{
    public static void main(String []arg){
       new Student();
       new Student("Tanooj",95.34);
       System.gc();
    }
}
