#include <iostream>
using namespace std;
class Book{
    public:
        virtual void display()=0;
        void hello(){}
};
class Notes : public Book{
    public:
        void display(){
                cout<<"Displayed Notes from Book";
        }
};
int main()
{
    Notes obj;
    obj.display();
    obj.hello();
    return 0;
}
