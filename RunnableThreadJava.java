import java.util.*;
class Table {
     void display(int n) {
        for (int i = 1; i <= 5; i++) {
            System.out.println(n * i);
        }
    }
}
class Table1 implements Runnable {
    Table t;
    Table1(Table t) {
        this.t = t;
    }
    public void run() {
        try {
            Scanner in = new Scanner(System.in);
            int num = in.nextInt();
            t.display(num);
        } catch (Exception e) {
            System.out.println("Invalid input! Please enter a valid number.");
            System.exit(0);
        }  
    }
}
class Table2 implements Runnable {
    Table t;
    Table2(Table t) {
        this.t = t;
    }
    public void run() {
        try {
            Scanner in = new Scanner(System.in);
            int num = in.nextInt();
            t.display(num);
        } catch (Exception e) {
            System.out.println("Invalid input! Please enter a valid number.");
            System.exit(0);
        }
    }  
}
public class RunnableThreadJava {
    public static void main(String[] args) {
        try {
            Table obj = new Table();
            Table1 t1 = new Table1(obj);
            Table2 t2 = new Table2(obj);
			 Thread th1 = new Thread(t1);
            Thread th2 = new Thread(t2);
            th1.start();
            th2.start();
        } catch (Exception e) {
            System.out.println("Invalid input! Please enter a valid number.");
            System.exit(0);
        }
    }
}
