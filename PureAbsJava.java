interface Base{
	void display();
}
class Child implements Base{
    public void display(){
        System.out.println("Pure abstract message");
    }
}
public class PureAbsJava{
    public static void main (String[] args){
        Child obj = new Child();
        obj.display();
    }
}