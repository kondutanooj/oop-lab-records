class Base{
	Base(){
	System.out.println("Call-Base");
	}
}
class Sub1 extends Base{
	Sub1(){
	System.out.println("Call-Sub1");
	}
}
class Sub2 extends Base{
	Sub2(){
	System.out.println("Call-Sub2");
	}
}
class Child extends Sub1,Sub2{
	Child(){
	System.out.println("Call-Child");
	}
}
public class MultipleInheriJAVA{
	public static void main(String[] args){
		Child obj = new Child();
	}
}