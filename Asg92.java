import java.awt.*;
import java.awt.event.*;

public class Asg92 extends Frame implements WindowListener {

    public Asg92() {
        super("Window Listener");
        addWindowListener(this);
        setSize(300, 200);
        setVisible(true);
    }

    public static void main(String[] args) {
        new Asg92();
    }

    public void windowOpened(WindowEvent e) {
        System.out.println("Window opened");
    }

    public void windowClosing(WindowEvent e) {
        System.out.println("Window closing");
        dispose();
    }

    public void windowClosed(WindowEvent e) {
        System.out.println("Window closed");
    }

    public void windowIconified(WindowEvent e) {
        System.out.println("Window iconified");
    }

    public void windowDeiconified(WindowEvent e) {
        System.out.println("Window deiconified");
    }

    public void windowActivated(WindowEvent e) {
        System.out.println("Window activated");
    }

    public void windowDeactivated(WindowEvent e) {
        System.out.println("Window deactivated");
    }
}
