#include <iostream>
using namespace std;
class Base{
   public:
      void add(int a, int b)
      {
            cout << "sum = " << (a + b);
      }
      void add(double a, double b)
      {
	    cout << endl << "sum = " << (a + b);
      }
};
int main()
{
        Base obj;
	obj.add(10, 2);
	obj.add(5.3, 6.2);
	return 0;
}
