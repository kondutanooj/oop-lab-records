#include<iostream>
using namespace std;
class Base {
private:
   int priValue;
protected:
   int proValue;
public:
   int pubValue;
   void getVal() {
       cout << "private value:"<< priValue << endl;
       cout << "protected value:"<< proValue << endl;
       cout <<"public value:"<< pubValue << endl;
   }
   int getpri(int priValue, int proValue, int pubValue){
       this->priValue = priValue;
       this->proValue = proValue;
       this->pubValue = pubValue;
       return priValue;
   }
   virtual void setVal(int priValue, int proValue, int pubValue);
};
void Base::setVal (int priValue,int  proValue,int pubValue){
	Base::getpri(priValue, proValue, pubValue);
       this->proValue = proValue;
       this->pubValue = pubValue;
}     
int main() {
   Base obj;
   int a,b,c;
   cin>>a>>b>>c;
   cout<<"The values before assignment are : \n"<<endl;
   obj.getVal();
   cout<<"The values after assignment are : \n"<<endl;
   obj.setVal(a,b,c);
   obj.getVal();
   return 0;
}
