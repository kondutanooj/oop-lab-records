class Base {
	int base = 0;
}
class Sub1 extends Base {
	int sub1 = 10;
}
class Sub2 extends Sub1 {
	int sub2 = 20;
}
class Sub3 extends Sub2 {
	int sub3 = 30;
}
class Sub4 extends Sub2 {
	int sub4 = 40;
}
public class InheriTypeJAVA{
	public static void main(String[] args){
		Sub1 obj1 = new Sub1();
		Sub2 obj2 = new Sub2();
		Sub3 obj3 = new Sub3();
		Sub4 obj4 = new Sub4();
		System.out.println("\n***SINGLE-INHERITANCE***\n");
		System.out.println(obj1.base+"\n"+obj1.sub1);
		System.out.println("\n***MULTI-LEVEL-INHERITANCE***\n");
		System.out.println(obj2.base+"\n"+obj2.sub1+"\n"+obj2.sub2);
		System.out.println("\n***HIERARCHICAL-INHERITANCE***\n");
		System.out.println("left-child");
		System.out.println(obj3.sub2+"\n"+obj3.sub3);
		System.out.println("right-child");
		System.out.println(obj4.sub2+"\n"+obj4.sub4);
		System.out.println("\n***HYBRID-INHERITANCE***\n");
		System.out.println("left-child");
		System.out.println(obj3.base+"\n"+obj3.sub1+"\n"+obj3.sub2+"\n"+obj3.sub3);
		System.out.println("right-child");
		System.out.println(obj4.base+"\n"+obj4.sub1+"\n"+obj4.sub2+"\n"+obj4.sub4);
	}
}