#include<iostream>
using namespace std;
class Box{
    private:
        float len,wid,high;
    public:
        inline void displayWelcomeMessage(){
            cout<<"Welcometo the CPP Program !!!"<<endl;}
        void boxArea(float length, float width){
            len=length;
            wid=width;
            cout<<"Area is : "<<len*wid<<endl;}
        void boxVolume(float length, float width, float height);
        friend class F;
};
void Box::boxVolume(float length, float width, float height){
    len=length;
    wid=width;
    high=height;
    cout<<"Volume is : "<<length*width*height<<endl;}
class F{
    public:
        void displayBoxDimensions(Box& d){
            cout<<"length : "<<d.len<<endl;
            cout<<"width : "<<d.wid<<endl;
            cout<<"height : "<<d.high<<endl;}
};
int main(){
    float l,w,h;
    Box obj;
    obj.displayWelcomeMessage();
    cout<<"Enter the dimensions: "<<endl;
    cin>>l>>w>>h;
    obj.boxArea(l,w);
    obj.boxVolume(l,w,h);
    F fri;
    fri.displayBoxDimensions(obj);
    return 0;
}
