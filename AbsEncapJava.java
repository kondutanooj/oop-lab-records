import java.util.*;

abstract class Base {
    private int priValue;
    protected int proValue;
    public int pubValue;
	
    void getVal() {
        System.out.println("private value : " + priValue);
        System.out.println("protected value : " + proValue);
        System.out.println("public value : " + pubValue);
    }
    int getpri(int priValue, int proValue, int pubValue) {
        this.priValue = priValue;
        this.proValue = proValue;
        this.pubValue = pubValue;
        return priValue;
    }

    abstract void setVal(int priValue, int proValue, int pubValue);
}
class Child extends Base {
    public void setVal(int priValue, int proValue, int pubValue) {
        super.getpri(priValue, proValue, pubValue);
        this.proValue = proValue;
        this.pubValue = pubValue;
	}
}

public class AbsEncapJava {
    public static void main(String[] args) {
        Child obj = new Child();
        Scanner i = new Scanner(System.in);
        int a = i.nextInt(); int b = i.nextInt(); int c = i.nextInt();
		System.out.println();
        System.out.println("The values before assignment are : ");
        obj.getVal();
		System.out.println();
        System.out.println("The values after assignment are : ");
        obj.setVal(a, b, c);
        obj.getVal();
        i.close();
    }
}
