#include<iostream>
using namespace std;
class Base{
	public:
	Base(){
	cout<<"Call-Base"<<endl;}
};
class Sub1:public Base{
	public:
	Sub1(){
	cout<<"Call-Sub1"<<endl;}
};
class Sub2:public Base{
	public:
	Sub2(){
	cout<<"Call-Sub2"<<endl;}
};
class Child:public Sub1, public Sub2{
	public:
	Child(){
	cout<<"Call-Child"<<endl;}
};
int main(){
Child obj;
return 0;
}