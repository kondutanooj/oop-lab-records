#include <iostream>
using namespace std;
class Test{
    public:
    template<class T>
    void add(T a,T b){
        cout<<a + b<<endl;
    }
};
int main()
{
    Test obj;
    obj.add(2,5);
    obj.add(2.3,2.5);
    return 0;
}
