#include<iostream>
using namespace std;

class Parent{
    public:
    void lastname(){
     cout<<"My Last name is KONDU"<<endl;}
};
class Child : public Parent{
    public:
    void name(){
     cout<<"My name is TANOOJ"<<endl;}
};
int main()
{
    Child obj;
    obj.name();
    obj.lastname();
    return 0;
}
